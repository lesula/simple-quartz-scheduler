package org.lesula;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Created by enrico on 10/20/15.
 */
public class App {

    private static Logger LOG = LogManager.getLogger(App.class);

    public static void main(String[] args){
        LOG.info("Starting scheduler");
        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.start();
            LOG.info("Scheduler started");
        } catch (SchedulerException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
