package org.lesula.jobs;

import org.quartz.*;

/**
 * Created by enrico on 10/20/15.
 */
public class MyJob implements InterruptableJob {
    @Override
    public void interrupt() throws UnableToInterruptJobException {

    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap dataMap = context.getTrigger().getJobDataMap();

        String param1 = dataMap.getString("param1");
        System.out.println(param1);
    }
}
